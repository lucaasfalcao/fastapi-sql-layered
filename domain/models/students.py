from sqlalchemy import Column, ForeignKey, Integer, String, UniqueConstraint
from sqlalchemy.orm import relationship

from domain.models.generic import GenericBase

class Student(GenericBase):
    __tablename__ = "alunos"

    id = Column(Integer, primary_key=True, autoincrement=True)
    cpf = Column(String, unique=True)
    nome = Column(String)
    data_aniversario = Column(String)
    rg = Column(String)
    orgao_expedidor = Column(String)
    curso_id = Column(Integer, ForeignKey("cursos.id")) 

    curso = relationship("Course", back_populates="")
    disciplinas = relationship("Class", \
     secondary='aluno_disciplina', back_populates="alunos")

class StudentClass(GenericBase):
     __tablename__ = "aluno_disciplina"
     __table_args__ = (
          UniqueConstraint('aluno_id', 'disciplina_id'),
     )

     id = Column(Integer, primary_key=True, autoincrement=True)

     aluno_id = Column(Integer, ForeignKey("alunos.id"))
     disciplina_id = Column(Integer, ForeignKey("disciplinas.id"))