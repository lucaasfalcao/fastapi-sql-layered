from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from domain.models.generic import GenericBase

class Teacher(GenericBase):
     __tablename__ = "professores"

     id = Column(Integer, primary_key=True, autoincrement=True)
     cpf = Column(String, unique=True)
     nome = Column(String)
     titulacao = Column(String)

     cursos = relationship("Course", back_populates="coordenador")
     disciplinas = relationship("Class", back_populates="professor")