from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from domain.models.generic import GenericBase

class Course(GenericBase):
     __tablename__ = "cursos"

     id = Column(Integer, primary_key=True, autoincrement=True)
     nome = Column(String, unique=True)
     ano_criacao = Column(Integer)
     predio = Column(String)
     coordenador_id = Column(Integer, ForeignKey("professores.id"))

     coordenador = relationship("Teacher", back_populates="cursos")
     alunos = relationship("Student", back_populates="curso")