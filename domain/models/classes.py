from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from domain.models.generic import GenericBase

class Class(GenericBase):
     __tablename__ = "disciplinas"

     id = Column(Integer, primary_key=True, autoincrement=True)
     nome = Column(String, unique=True)
     descricao = Column(String)
     codigo = Column(String)
     professor_id = Column(Integer, ForeignKey("professores.id"))

     professor = relationship("Teacher", back_populates="disciplinas")
     alunos = relationship("Student", \
          secondary='aluno_disciplina', back_populates="disciplinas")