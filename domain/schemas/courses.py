from pydantic import Field

from domain.schemas.generic import GenericModel
from domain.schemas.teachers import Teacher

class CourseBase(GenericModel):
    nome: str = Field(title='Name of Course')
    ano_criacao: int = Field(title='Year of course creation')
    predio: str = Field(title='Course building name')

    class Config:
        schema_extra = {
            "example": {
                "nome": "Engenharia Civil",
                "ano_criacao": "1987",
                "predio":  "CTEC"
                }
            }

class CourseCreate(CourseBase):
    pass

class Course(CourseBase):
    id: int = Field(title='ID of course')
    coordenador: Teacher | None = Field(title='Coordinator of the course')

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True
        schema_extra = {
            "example": {
                "nome": "Engenharia Civil",
                "ano_criacao": "1987",
                "predio":  "CTEC",
                "id": 0,
                "coordenador": {
                    "cpf": "000.000.000-00",
                    "nome": "Barbirato",
                    "titulacao": "Doutorado",
                    "id": 0
                }
            }
        }