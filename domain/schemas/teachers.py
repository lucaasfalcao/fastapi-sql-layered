from pydantic import Field

from domain.schemas.generic import GenericModel

class TeacherBase(GenericModel):
    cpf: str = Field(title="CPF of teacher", regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')
    nome: str  = Field(title='Name of teacher')
    titulacao: str = Field(title='Teacher titration')

    class Config:
        schema_extra = {
            "example": {
                "cpf": "000.000.000-00",
                "nome": "Roberto",
                "titulacao": "Doutorado"
                    }
                }

class TeacherCreate(TeacherBase):
    pass

class Teacher(TeacherBase):
    id: int = Field(title='ID of teacher')

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True
        schema_extra = {
            "example": {
                "cpf": "000.000.000-00",
                "nome": "Roberto",
                "titulacao": "Doutorado",
                "id": 0
                    }
                }