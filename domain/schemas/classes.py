from pydantic import Field

from domain.schemas.generic import GenericModel
from domain.schemas.teachers import Teacher

class ClassBase(GenericModel):
    nome: str = Field(title='Name of class')
    descricao: str = Field(title='Description of class')
    codigo: str = Field(title='Code of class')

    class Config:
        schema_extra = {
            "example": {
                "nome": "Cálculo 1",
                "descricao": "Limites, derivadas, integrais, etc",
                "codigo": "MAT-01"
                    }
                }

class ClassCreate(ClassBase):
    pass

class Class(ClassBase):
    id: int = Field(title='ID of class')
    professor: Teacher | None = Field(title='Class teacher')

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True
        schema_extra = {
            "example": {
                "nome": "Cálculo 1",
                "descricao": "Limites, derivadas, integrais, etc",
                "codigo": "MAT-01",
                "id": 0,
                "professor": {
                    "cpf": "000.000.000-00",
                    "nome": "Roberto",
                    "titulacao": "Doutorado",
                    "id": 0
                }
            }
        }
