from pydantic import Field
from datetime import date

from domain.schemas.generic import GenericModel
from domain.schemas.courses import Course
from domain.schemas.classes import Class, ClassBase

class StudentBase(GenericModel):
    cpf: str = Field(title='CPF of student', regex='[0-9]{3}\.?[0-9]{3}\.?[0-9]{3}\-?[0-9]{2}')
    nome: str = Field(title='Name of student', example='Jane Smith')
    data_aniversario: date | None = Field(default=None, title='Birth date of student')
    rg: str | None = Field(title='Number of identity card')
    orgao_expedidor: str | None = Field(title='Expediter of identity card')

    class Config:
        schema_extra = {
            "example": {
                "cpf":  "000.000.000-00",
                "nome": "Jane Smith",
                "data_aniversario": "1970-01-01",
                "rg": "0000000-0",
                "orgao_expedidor": "SSP"
                }
            }

class StudentCreate(StudentBase):
    pass

class Student(StudentBase):
    id: int = Field(title='ID of student')
    curso: Course | None = Field(title='Course of the student')
    disciplinas: list[Class] | None = Field(title='Classes of the student')

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True
        schema_extra = {
            "example": {
                "cpf": "000.000.000-00",
                "nome": "Jane Smith",
                "data_aniversario": "1970-01-01",
                "rg": "0000000-0",
                "orgao_expedidor": "SSP",
                "id": 0,
                "curso": {
                    "nome": "Engenharia Civil",
                    "ano_criacao": "1987",
                    "predio": "CTEC",
                    "id": 0
                },
                "disciplinas": [
                    {
                        "nome": "Cálculo 1",
                        "descricao": "Limites, derivadas, integrais, etc",
                        "codigo": "MAT-01",
                        "id": 0
                    },
                    {
                        "nome": "Mecânica dos Sólidos",
                        "descricao": "Elasticidade, tração, flexão, etc",
                        "codigo": "ECIV-51",
                        "id": 0
                    }
                ]
            }
        }

class StudentClass(GenericModel):
    disciplina: ClassBase | None = Field(title='Class')
    alunos: list[StudentBase] | None = Field(title='Students of the class')

    class Config:
        orm_mode = True
        arbitrary_types_allowed = True
