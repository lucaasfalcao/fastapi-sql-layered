from sqlalchemy.orm import Session

from domain.models.classes import Class
from domain.models.students import Student, StudentClass
from domain.schemas.classes import ClassCreate

def get_classes(db:Session):
    return db.query(Class).all()

def get_class(db: Session, class_id: int):
    return db.query(Class).filter(Class.id == class_id).first()

def get_class_by_name(db: Session, class_name: str):
    return db.query(Class).filter(Class.nome == class_name).first()

def create_class(db: Session, classe: ClassCreate):
    db_class = Class(nome=classe.nome, descricao=classe.descricao, codigo=classe.codigo)
    db.add(db_class)
    db.commit()
    db.refresh(db_class)
    return db_class

def change_class_teacher(class_id: int, teacher_id: int, db: Session):
    classe = get_class(db, class_id)
    #get_teacher(teacher_id, db)
    db.query(Class).filter(Class.id == class_id).update({'professor_id': teacher_id})
    db.commit()
    db.refresh(classe)
    return classe

def get_class_students(db: Session, class_id: int):
    return db.query(Student).filter(Student.id == StudentClass.aluno_id).filter(StudentClass.disciplina_id == class_id).all()