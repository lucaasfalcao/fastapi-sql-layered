from sqlalchemy.orm import Session

from domain.models.students import Student, StudentClass
from domain.schemas.students import StudentCreate

def get_students(db:Session):
    return db.query(Student).all()

def get_student(db: Session, student_id: int):
    return db.query(Student).filter(Student.id == student_id).first()

def get_student_by_cpf(db: Session, student_cpf: str):
    return db.query(Student).filter(Student.cpf == student_cpf).first()

def create_student(db: Session, student: StudentCreate):
    db_student = Student(cpf=student.cpf, nome=student.nome, data_aniversario=student.data_aniversario, rg=student.rg, orgao_expedidor=student.orgao_expedidor)
    db.add(db_student)
    db.commit()
    db.refresh(db_student)
    return db_student

def change_student_course(student_id: int, course_id: int, db: Session):
    student = get_student(db, student_id)
    db.query(Student).filter(Student.id == student_id).update({'curso_id': course_id})
    db.commit()
    db.refresh(student)
    return student

def add_student_class(student_id: int, class_id: int, db: Session):
    student = get_student(db, student_id)
    student_class = StudentClass(aluno_id=student_id, disciplina_id=class_id)
    db.add(student_class)
    db.commit()
    db.refresh(student)
    return student

def rem_student_class(student_id: int, class_id: int, db: Session):
    student = get_student(db, student_id)
    db.query(StudentClass).filter(StudentClass.aluno_id == student_id).filter(StudentClass.disciplina_id == class_id).delete()
    db.commit()
    db.refresh(student)
    return student