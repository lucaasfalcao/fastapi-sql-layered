from sqlalchemy.orm import Session

from domain.models.courses import Course
from domain.models.students import Student
from domain.schemas.courses import CourseCreate

def get_courses(db:Session):
    return db.query(Course).all()

def get_course(db: Session, course_id: int):
    return db.query(Course).filter(Course.id == course_id).first()

def get_course_by_name(db: Session, course_name: str):
    return db.query(Course).filter(Course.nome == course_name).first()

def create_course(db: Session, course: CourseCreate):
    db_course = Course(nome=course.nome, ano_criacao=course.ano_criacao, predio=course.predio)
    db.add(db_course)
    db.commit()
    db.refresh(db_course)
    return db_course

def change_course_coordinator(course_id: int, teacher_id: int, db: Session):
    course = get_course(db, course_id)
    db.query(Course).filter(Course.id == course_id).update({'coordenador_id': teacher_id})
    db.commit()
    db.refresh(course)
    return course
    
def get_course_students(db: Session, course_id: int):
    return db.query(Student).filter(Student.curso_id == course_id).all()