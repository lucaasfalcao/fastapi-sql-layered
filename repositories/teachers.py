from sqlalchemy.orm import Session

from domain.models.teachers import Teacher
from domain.schemas.teachers import TeacherCreate

def get_teachers(db:Session):
    return db.query(Teacher).all()

def get_teacher(db: Session, teacher_id: int):
    return db.query(Teacher).filter(Teacher.id == teacher_id).first()

def get_teacher_by_cpf(db: Session, teacher_cpf: str):
    return db.query(Teacher).filter(Teacher.cpf == teacher_cpf).first()

def create_teacher(db: Session, teacher: TeacherCreate):
    db_teacher = Teacher(cpf=teacher.cpf ,nome=teacher.nome, titulacao=teacher.titulacao)
    db.add(db_teacher)
    db.commit()
    db.refresh(db_teacher)
    return db_teacher