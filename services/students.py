from fastapi import HTTPException
from sqlalchemy.orm import Session

from domain.schemas.students import StudentCreate
from repositories import students as students_repository

def get_students(db: Session):
    db_students = students_repository.get_students(db)
    if db_students is None:
        raise HTTPException(status_code=404, detail="Students not found.")
    return db_students

def get_student(student_id: int, db: Session):
    db_student = students_repository.get_student(db, student_id)
    if db_student is None:
        raise HTTPException(status_code=404, detail="Student not found.")
    return db_student

def create_student(db: Session, student: StudentCreate):
    db_student = students_repository.get_student_by_cpf(db, student.cpf)
    if db_student:
        raise HTTPException(status_code=400, detail="CPF already registered.")
    return students_repository.create_student(db, student)

def change_student_course(student_id: int, course_id: int, db: Session):
    db_student = students_repository.change_student_course(student_id, course_id, db)
    if db_student is None:
        raise HTTPException(status_code=404, detail="Student not found.")
    return db_student

def add_student_class(student_id: int, class_id: int, db: Session):
    db_student = students_repository.add_student_class(student_id, class_id, db)
    if db_student is None:
        raise HTTPException(status_code=404, detail="Student not found.")
    return db_student

def rem_student_class(student_id: int, class_id: int, db: Session):
    db_student = students_repository.rem_student_class(student_id, class_id, db)
    if db_student is None:
        raise HTTPException(status_code=404, detail="Student not found.")
    return db_student