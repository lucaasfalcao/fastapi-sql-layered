from fastapi import HTTPException
from sqlalchemy.orm import Session

from domain.schemas.teachers import TeacherCreate
from repositories import teachers as teachers_repository

def get_teachers(db: Session):
    db_teachers = teachers_repository.get_teachers(db)
    if db_teachers is None:
        raise HTTPException(status_code=404, detail="Teachers not found.")
    return db_teachers

def get_teacher(teacher_id: int, db: Session):
    db_teacher = teachers_repository.get_teacher(db, teacher_id)
    if db_teacher is None:
        raise HTTPException(status_code=404, detail="Teacher not found.")
    return db_teacher

def create_teacher(teacher: TeacherCreate, db: Session):
    db_teacher = teachers_repository.get_teacher_by_cpf(db, teacher.cpf)
    if db_teacher:
        raise HTTPException(status_code=400, detail="CPF already registered.")
    return teachers_repository.create_teacher(db, teacher)