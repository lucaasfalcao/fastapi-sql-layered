from fastapi import HTTPException
from sqlalchemy.orm import Session

from domain.schemas.classes import ClassCreate
from repositories import classes as classes_repository

def get_classes(db: Session):
    db_classes = classes_repository.get_classes(db)
    if db_classes is None:
        raise HTTPException(status_code=404, detail="Classes not found.")
    return db_classes

def get_class(class_id: int, db: Session):
    db_class = classes_repository.get_class(db, class_id)
    if db_class is None:
        raise HTTPException(status_code=404, detail="Class not found.")
    return db_class

def create_class(classe: ClassCreate, db: Session):
    db_class = classes_repository.get_class_by_name(db, classe.nome)
    if db_class:
        raise HTTPException(status_code=400, detail="Class name already registered.")
    return classes_repository.create_class(db, classe)

def change_class_teacher(class_id: int, teacher_id: int, db: Session):
    db_class= classes_repository.change_class_teacher(class_id, teacher_id, db)
    if db_class is None:
        raise HTTPException(status_code=404, detail="Class not found.")
    return db_class

def get_class_students(class_id: int, db: Session):
    db_students = classes_repository.get_class_students(db, class_id)
    if db_students is None:
        raise HTTPException(status_code=404, detail="Students not found.")
    return db_students