from fastapi import HTTPException
from sqlalchemy.orm import Session

from domain.schemas.courses import CourseCreate
from repositories import courses as courses_repository

def get_courses(db: Session):
    db_courses = courses_repository.get_courses(db)
    if db_courses is None:
        raise HTTPException(status_code=404, detail="Courses not found.")
    return db_courses

def get_course(course_id: int, db: Session):
    db_course = courses_repository.get_course(db, course_id)
    if db_course is None:
        raise HTTPException(status_code=404, detail="Course not found.")
    return db_course

def create_course(course: CourseCreate, db: Session):
    db_course = courses_repository.get_course_by_name(db, course.nome)
    if db_course:
        raise HTTPException(status_code=400, detail="Course name already registered.")
    return courses_repository.create_course(db, course)

def change_course_coordinator(course_id: int, teacher_id: int, db: Session):
    db_course = courses_repository.change_course_coordinator(course_id, teacher_id, db)
    if db_course is None:
        raise HTTPException(status_code=404, detail="Course not found.")
    return db_course

def get_course_students(course_id: int, db: Session):
    db_students = courses_repository.get_course_students(db, course_id)
    if db_students is None:
        raise HTTPException(status_code=404, detail="Students not found.")
    return db_students
