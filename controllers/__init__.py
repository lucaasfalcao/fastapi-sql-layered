from . import students
from . import courses
from . import teachers
from . import classes

routes = [
    students.router,
    courses.router,
    teachers.router,
    classes.router
]