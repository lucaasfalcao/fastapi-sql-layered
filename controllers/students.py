from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from config.database import get_db
from domain.schemas.students import Student, StudentCreate
from services import students as students_service

router = APIRouter()

STUDENT_TAG = 'Students'

@router.get(
    "/students",
    summary="Fetch all students data",
    tags=[STUDENT_TAG],
    response_model=list[Student],
)
def read_students(db: Session = Depends(get_db)):
    return students_service.get_students(db)

@router.get(
    "/students/{student_id}",
    summary="Fetch student data",
    tags=[STUDENT_TAG],
    response_model=Student,
)
def read_student(student_id: int, db: Session = Depends(get_db)):
    return students_service.get_student(student_id, db)

@router.post(
    "/students",
    summary='Add a new student',
    tags=[STUDENT_TAG],
    response_model=Student,
)
def add_student(student: StudentCreate, db: Session = Depends(get_db)):
    return students_service.create_student(db, student)

@router.put(
    '/students/{student_id}/course/{course_id}',
    summary="Set or change student's course",
    tags=[STUDENT_TAG],
    response_model=Student,
)
def update_student_course(student_id: int, course_id: int, db: Session = Depends(get_db)):
    return students_service.change_student_course(student_id, course_id, db)

@router.post(
    '/students/{student_id}/class/{class_id}',
    summary='Add student to a class',
    tags=[STUDENT_TAG],
    response_model=Student,
)
def add_student_class(student_id: int, class_id: int, db: Session = Depends(get_db)):
    return students_service.add_student_class(student_id, class_id, db)

@router.delete(
    '/students/{student_id}/class/{class_id}',
    summary='Remove student from a class',
    tags=[STUDENT_TAG],
    response_model=Student,
)
def delete_student_class(student_id: int, class_id: int, db: Session = Depends(get_db)):
    return students_service.rem_student_class(student_id, class_id, db)