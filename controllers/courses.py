from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from config.database import get_db
from domain.schemas.courses import Course, CourseCreate
from domain.schemas.students import Student
from services import courses as courses_service

router = APIRouter()

COURSE_TAG = 'Courses'

@router.get(
    "/courses",
    summary="Fetch all courses data",
    tags=[COURSE_TAG],
    response_model=list[Course],
)
def read_courses(db: Session = Depends(get_db)):
    return courses_service.get_courses(db)

@router.get(
    "/courses/{course_id}",
    summary="Fetch course data",
    tags=[COURSE_TAG],
    response_model=Course,
)
def read_course(course_id: int, db: Session = Depends(get_db)):
    return courses_service.get_course(course_id, db)

@router.post(
    "/courses",
    summary='Add a new course',
    tags=[COURSE_TAG],
    response_model=Course,
)
def add_course(course: CourseCreate, db: Session = Depends(get_db)):
    return courses_service.create_course(course, db)

@router.put(
    '/courses/{course_id}/coordinator/{teacher_id}',
    summary="Set or change a teacher as the course coordinator",
    tags=[COURSE_TAG],
    response_model=Course,
)
def update_course_coordinator(course_id: int, teacher_id: int, db: Session = Depends(get_db)):
    return courses_service.change_course_coordinator(course_id, teacher_id, db)

@router.get(
    "/courses/{course_id}/students",
    summary="Fetch students of course",
    tags=[COURSE_TAG],
    response_model=list[Student],
)
def read_course_students(course_id: int, db: Session = Depends(get_db)):
    return courses_service.get_course_students(course_id, db)
