from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from config.database import get_db
from domain.schemas.classes import Class, ClassCreate
from domain.schemas.students import Student
from services import classes as classes_service

router = APIRouter()

CLASS_TAG = 'Classes'

@router.get(
    "/classes",
    summary="Fetch all classes data",
    tags=[CLASS_TAG],
    response_model=list[Class],
)
def read_classes(db: Session = Depends(get_db)):
    return classes_service.get_classes(db)

@router.get(
    "/classes/{class_id}",
    summary="Fetch class data",
    tags=[CLASS_TAG],
    response_model=Class,
)
def read_class(class_id: int, db: Session = Depends(get_db)):
    return classes_service.get_class(class_id, db)

@router.post(
    "/classes",
    summary='Add a new class',
    tags=[CLASS_TAG],
    response_model=Class,
)
def add_class(classe: ClassCreate, db: Session = Depends(get_db)):
    return classes_service.create_class(classe, db)

@router.put(
    '/classes/{class_id}/teacher/{teacher_id}',
    summary="Set or change a teacher as the class teacher",
    tags=[CLASS_TAG],
    response_model=Class,
)
def update_class_teacher(class_id: int, teacher_id: int, db: Session = Depends(get_db)):
    return classes_service.change_class_teacher(class_id, teacher_id, db)

@router.get(
    "/classes/{class_id}/students",
    summary="Fetch students of class",
    tags=[CLASS_TAG],
    response_model=list[Student],
)
def read_class_students(class_id: int, db: Session = Depends(get_db)):
    return classes_service.get_class_students(class_id, db)
