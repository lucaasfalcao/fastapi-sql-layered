from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from config.database import get_db
from domain.schemas.teachers import Teacher, TeacherCreate
from services import teachers as teachers_service

router = APIRouter()

TEACHER_TAG = 'Teachers'

@router.get(
    "/teachers",
    summary="Fetch all teachers data",
    tags=[TEACHER_TAG],
    response_model=list[Teacher],
)
def read_teachers(db: Session = Depends(get_db)):
    return teachers_service.get_teachers(db)

@router.get(
    "/teachers/{teacher_id}",
    summary="Fetch teacher data",
    tags=[TEACHER_TAG],
    response_model=Teacher,
)
def read_teacher(teacher_id: int, db: Session = Depends(get_db)):
    return teachers_service.get_teacher(teacher_id, db)

@router.post(
    "/teachers",
    summary='Add a new teacher',
    tags=[TEACHER_TAG],
    response_model=Teacher,
)
def add_teacher(teacher: TeacherCreate, db: Session = Depends(get_db)):
    return teachers_service.create_teacher(teacher, db)