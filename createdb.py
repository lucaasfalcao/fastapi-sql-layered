from config.database import engine
from domain.models.generic import GenericBase

from domain.models import (
    students,
    courses,
    teachers,
    classes,
)

GenericBase.metadata.create_all(bind=engine)